# !/usr/bin/python
# -*- coding: utf8 -*-
"""
Explicação:
Dada uma matriz de tamanho MxN na qual cada elemento represente um pixel, crie
um programa que leia uma sequência de comandos e os interprete manipulando a
matriz de acordo com a descrição abaixo de cada comando.

Comandos:
I M N
Cria uma nova matriz MxN. Todos os pixels são brancos (O).

C
Limpa a matriz. O tamanho permanece o mesmo. Todos os pixels ficam brancos (O).

L X Y C
Colore um pixel de coordenadas (X,Y) na cor C.

V X Y1 Y2 C
Desenha um segmento vertical na coluna X nas linhas de Y1 a Y2 (intervalo
inclusivo) na cor C.

H X1 X2 Y C
Desenha um segmento horizontal na linha Y nas colunas de X1 a X2 (intervalo
inclusivo) na cor C.

K X1 Y1 X2 Y2 C
Desenha um retangulo de cor C. (X1,Y1) é o canto superior esquerdo e (X2,Y2) o
canto inferior direito.

F X Y C
Preenche a região com a cor C. A região R é definida da seguinte forma:
O pixel (X,Y) pertence à região. Outro pixel pertence à região, se e somente se,
ele tiver a mesma cor que o pixel (X,Y) e tiver pelo menos um lado em comum com
um pixel pertencente à região.

S Name
Escreve a imagem em um arquivo de nome Name.

X
Encerra o programa.

Considerações:
Comandos diferentes de I, C, L, V, H, K, F, S e X devem ser ignorados

Testes:

Entrada 01
I 5 6
L 2 3 A
S one.bmp
G 2 3 J
V 2 3 4 W
H 3 4 2 Z
F 3 3 J
S two.bmp
X

Saida 01
one.bmp
OOOOO
OOOOO
OAOOO
OOOOO
OOOOO
OOOOO
two.bmp
JJJJJ
JJZZJ
JWJJJ
JWJJJ
JJJJJ
JJJJJ

Entrada 02
I 10 9
L 5 3 A
G 2 3 J
V 2 3 4 W
H 1 10 5 Z
F 3 3 J
K 2 7 8 8 E
F 9 9 R
S one.bmp
X

Saida 02
one.bmp
JJJJJJJJJJ
JJJJJJJJJJ
JWJJAJJJJJ
JWJJJJJJJJ
ZZZZZZZZZZ
RRRRRRRRRR
REEEEEEERR
REEEEEEERR
RRRRRRRRRR
"""
def main():
  cmd = [0] #Inicialização da variável de comando

  while cmd[0] != 'X':

    cmd = input('Digite um comando:') #Requisição do comando

    #Comando que gera a matriz MxN
    if cmd[0]=='I':
      M = int(cmd[2]) #Numero de linhas
      N = int(cmd[4]) #Numero de colunas
      matrix = M*[N*['O']] 

    #Comando que limpa a matriz
    if cmd[0]=='C':
      matrix = M*[N*['O']]

    #Comando que altera a cor de um ponto da matriz
    if cmd[0]=='L':
      aux=len(matrix[0])*['O']
      aux[int(cmd[4])]=cmd[6]
      matrix[int(cmd[2])] = aux

    #Comando que muda a cor de uma linha vertical 
    if cmd[0]=='V': 
      aux=len(matrix[0])*['O']
      aux[int(cmd[2])]=cmd[8]
      for i in range(int(cmd[4]),int(cmd[6])+1):
          matrix[i] = aux

    #Comando que muda a cor de uma linha horizontal      
    if cmd[0]=='H':
      aux=len(matrix[0])*['O']
      for i in range(int(cmd[2]),int(cmd[4])+1):
          aux[i]=cmd[8]
      matrix[int(cmd[6])] = aux
    #Comando para desenhar retangulo
    if cmd[0]=='K':
        #Criando linha horizontal superior
        aux=len(matrix[0])*['O']
        for i in range(int(cmd[4]),int(cmd[8])+1):
            aux[i]=cmd[10]
        matrix[int(cmd[2])] = aux
        #Criando linha horizontal inferior
        aux=len(matrix[0])*['O']
        for i in range(int(cmd[4]),int(cmd[8])+1):
            aux[i]=cmd[10]
        matrix[int(cmd[6])] = aux
        #Criando linhas verticais
        aux=len(matrix[0])*['O']
        aux[int(cmd[4])]=cmd[10]
        aux[int(cmd[8])]=cmd[10]
        for i in range(int(cmd[2])+1,int(cmd[6])):
            matrix[i]=aux

  print(matrix)


#if __name__ == '__main__':
main()
